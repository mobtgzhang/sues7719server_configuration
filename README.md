<!-- Author: Zhang Geng -->
<!-- Email: mobtgzhang@outlook.com -->

# SUES 7719 实验室服务器配置手册

本项目用于SUES 7719实验室服务器配置和使用的手册，目前还在更新中，包含有以下的几个章节：
- [x] 操作系统安装；
- [x] SSH远程登录配置；
- [x] 深度学习环境配置；
- [ ] 深度学习框架环境安装
- [x] 远程登录工具使用；
- [x] Docker使用；
- [x] WSL2使用；
- [ ] LaTeX安装教程；
- [ ] 大模型部署方法。

## OverLeaf 地址
[OverLeaf](https://www.overleaf.com/read/jybwscvybvmt#82b66b)

## License

本模板发布遵循GNU General Public License v3.0 。


